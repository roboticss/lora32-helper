#include <WiFi.h>
#include <functional>
#include "SSD1306.h"

void wifiConnect(char* ssid, char *password, int timeout, std::function<void(boolean)> callback) {
  WiFi.begin(ssid, password);

  int out = 0;
  
  while (WiFi.status() != WL_CONNECTED && out < timeout) {    
    delay(500);
    Serial.print(".");
    out += 500;
  }

  callback(out < timeout);
}

void initDisplay(SSD1306* display) {
  (*display).init();

  (*display).flipScreenVertically();
  (*display).setFont(ArialMT_Plain_10);
}

void centerHead(String text, SSD1306* display) {
  (*display).setFont(ArialMT_Plain_16);
  (*display).drawString(0, 26, text);
}

void centerSubHead(String text, SSD1306* display) {
  (*display).setFont(ArialMT_Plain_10);
  (*display).drawString(0, 45, text);
}
