from telethon import TelegramClient, events
import requests

LORA_HOST='192.168.1.2'

credentials=open('credentials.txt', 'r').read().split('\n')

# api_hash, api_hash from https://my.telegram.org
api_id = credentials[0]
api_hash = credentials[1]
session_name = credentials[2]

client = TelegramClient(session_name, api_id, api_hash)

def send_to_lora(method):
	return requests.get('http://' + LORA_HOST + '/' + method)

@client.on(events.NewMessage)
async def my_event_handler(event):
	print('kek')

client.start()
client.run_until_disconnected()