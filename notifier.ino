#include "service.h"

char* reserveSsid = "LORA_ESP_32";
char* reservePassword = "LORA_ESP_32";

char* ssid     = "MLink";
char* password = "PASSWORD";

void setup() {
  Serial.begin(115200);
  delay(10);

  initDisplay(&display);

  display.clear();
  centerHead("Connecting wifi..", &display);
  centerSubHead(ssid, &display);
  display.display();

  wifiConnect(ssid, password, 2000, [](boolean success) {
    if (success) {
      Serial.println("");
      Serial.println("WiFi connected");
      Serial.println("IP address: "); 
      Serial.println(WiFi.localIP());
      display.clear();
      centerHead("Wifi connected!", &display);
      centerSubHead(WiFi.localIP().toString(), &display);
      display.display();
    } else {
      display.clear();
      centerHead("Connecting wifi..", &display);
      centerSubHead(reserveSsid, &display);
      display.display();
      
      wifiConnect(reserveSsid, reservePassword, 20000, [](boolean success) {
        if (success) {
          Serial.println("");
          Serial.println("WiFi connected");
          Serial.println("IP address: "); 
          Serial.println(WiFi.localIP());
          display.clear();
          centerHead("Wifi connected!", &display);
          centerSubHead(WiFi.localIP().toString(), &display);
          display.display();
        } else {
          display.clear();
          centerHead("WIFI FAILED!", &display);
          display.display();
        }
      });
    }
  });
}

void loop() {
  // put your main code here, to run repeatedly:

}
