#include <WiFi.h>
#include <functional>
#include "SSD1306.h"


/* -- init display object -- */
#define SDA    4
#define SCL   15
#define RST   16

SSD1306  display(0x3c, SDA, SCL, RST);

void initDisplay(SSD1306* display);

void centerHead(String text, SSD1306* display);
void centerSubHead(String text, SSD1306* display);
/* -- end init display object -- */

void wifiConnect(char* ssid, char *password, int timeout, std::function<void(boolean)> callback);

void makeRequest(String method, String url, std::function<void(String)> callback);
